<?php
require_once('model.php');
class JobsModel extends Model
{
	private $_currentpage = 1;
	static $_perpage = 5;
	public function Prepare()
	{		
		$sql = "SELECT
					`PrimaryKey`, 
					`job`, 
					`job_action`, 
					`advertisername`, 
					`advertisertype`, 
					`advertisertype_valueid`, 
					`senderreference`, 
					`displayreference`, 
					`classification`, 
					`classification_valueid`, 
					`position`, 
					SUBSTRING(`description`,1,100) AS `description`, 
					`country`, 
					`country_valueid`, 
					`location`, 
					`location_valueid`, 
					`area`, 
					`area_valueid`, 
					`postalcode`, 
					`applicationurl`, 
					`descriptionurl`, 
					`language`, 
					`language_valueid`, 
					`contactname`, 
					`employmenttype`, 
					`employmenttype_valueid`, 
					`startdate`, 
					`duration`, 
					`workhours`, 
					`workhours_valueid`, 
					`salarycurrency`, 
					`salarycurrency_valueid`, 
					`salaryminimum`, 
					`salarymaximum`, 
					`salaryperiod`, 
					`salaryperiod_valueid`, 
					`salaryadditional`, 
					`jobsource`, 
					`jobsourceurl`, 
					`videolinkurl` 
				FROM 
					`xml_jobg8_oz` 
				WHERE 1 ";
		if(isset($_POST))
		{
			$Keywords = $_POST['Keywords'];
			if($Keywords!='')
			{
				$sql .= "AND MATCH(`description`) AGAINST (:keyword IN NATURAL LANGUAGE MODE) "; 
				$this->_params[':keyword'] = $Keywords;
			}
			$worktype = $_POST['worktype'];
			if(array_key_exists('arrworktype',$_POST))
			{
				$worktypein = $_POST['arrworktype'];
				$fmt = "AND `employmenttype_valueid` IN (%s) ";
				$sql .= sprintf($fmt,$worktypein);				
			}
			else if($worktype!=0)
			{
				$sql .= "AND `employmenttype_valueid` = :worktype ";
				$this->_params[':worktype'] = $worktype;
			}
			$recruiters = $_POST['recruiters'];
			if($recruiters!='0')
			{
				$sql .= "AND `advertisername` = :recruiters AND `advertisertype_valueid` IN (15890,15891) ";
				$this->_params[':recruiters'] = $recruiters;
			}
			$industry = $_POST['industry'];
			if($industry!=0)
			{
				$sql .= "AND `classification_valueid` = :industry ";
				$this->_params[':industry'] = $industry;
			}
			$occupation = $_POST['occupation'];
			if($occupation!='0')
			{
				$sql .= "AND `position` = :occupation ";
				$this->_params[':occupation'] = $occupation;
			}
			$employers = $_POST['employers'];
			if($employers!='0')
			{
				$sql .= "AND `advertisername` = :employers AND `advertisertype_valueid` IN (15892) ";
				$this->_params[':employers'] = $employers;
			}
			if(array_key_exists('advertiser',$_POST))
			{
				$sql .= "AND `advertisername` = :advertiser ";
				$this->_params[':advertiser'] = $_POST['advertiser'];
			}
			$parentlocation = $_POST['parentlocation'];
			if($parentlocation!=0)
			{
				$sql .= "AND `location_valueid` = :parentlocation ";
				$this->_params[':parentlocation'] = $parentlocation;
			}
			if(array_key_exists('childlocation',$_POST))
			{
				$childlocation = $_POST['childlocation'];
				if($childlocation!=0)
				{
					$sql .= "AND `area_valueid` = :childlocation ";
					$this->_params[':childlocation'] = $childlocation;
				}
			}			
			if(array_key_exists('JobSearchBox:chkUnspecifiedchildlocation',$_POST))
			{
				$JobSearchBox = $_POST['JobSearchBox:chkUnspecifiedchildlocation'];				
			}
		}
		$this->_sql = $sql;
	}

	public function getPagination()
	{
		$total = $this->Count($this->_sql,$this->_params);
		if(isset($_POST['page']) && $_POST['page']>1)
			$this->_currentpage = $_POST['page'];
		$perpage = self::$_perpage;
		$page = array();
		$page['total'] = $total;
		$page['totalpage'] = ceil($total/$perpage);
		$page['startpage'] = 1;
		$page['endpage'] = $page['totalpage'];
		if($page['totalpage']>10)
		{
			if($this->_currentpage>5)
			{
				$page['startpage'] = $this->_currentpage-5;
				if($this->_currentpage+5<$page['totalpage'])
					$page['endpage'] = $this->_currentpage+5;
			}
			else
				$page['endpage'] = 10;
		}
		if($total>0)
		{			
			$page['currentpage'] = $this->_currentpage;
			if($this->_currentpage < $page['totalpage'])
				$page['next'] = true;
			if($this->_currentpage > 1)
				$page['prev'] = true;
		}		
		return $page;			
	}
	
	public function getJobs()
	{
		$offset = ($this->_currentpage-1)*self::$_perpage;
		$sql = $this->_sql . ' LIMIT '.$offset.', '.self::$_perpage;
		return $this->query($sql,$this->_params);
	}	
	
	public function ClassificationList()
	{
		$list = $this->CountGroup($this->_sql,'classification', 'classification_valueid',$this->_params);
		return $list;
	}
	public function OccupationList()
	{
		$list = $this->CountGroup($this->_sql,'position', 'position',$this->_params);
		return $list;
	}
	
	public function LocationList()
	{
		$list = $this->CountGroup($this->_sql,'location','location_valueid',$this->_params);
		return $list;
	}
	public function AreaList()
	{
		$list = $this->CountGroup($this->_sql,'area', 'area_valueid',$this->_params);
		return $list;
	}
	public function WorkTypeList()
	{
		$list = $this->CountGroup($this->_sql,'employmenttype','employmenttype_valueid',$this->_params);
		return $list;
	}
}