<div class="jobapply-apply mod-section state-section-page l-clearfix l-row">
<div class="state-alignright state-bottom-border">
	<span class="state-floatleft advertiserLabel">Advertiser:&nbsp;</span><span class="state-floatleft advertiserText"><?=$this->job->advertisername?></span>
	<span class="state-alignright moreAdvertiser"><a href="#" target="<?=$this->job->advertisername?>">More jobs by this advertiser</a></span>
    <i class="ico ico-23 ico-magnify"></i><a href="#" class="goback">back to results</a>
</div>

    <div class="state-floatleft grid_6 state-job-ad"> 
    <h1><?=$this->job->position?></h1>
	<?php if($this->job->applicationurl==''): ?>   
<form method="post" id="SubmitForm" enctype="multipart/form-data" action="/Apply/22588962" novalidate="novalidate">
    <fieldset id="ContactDetails">
        <legend>Contact Details</legend>
        <ul>
            <li>
                <label for="FirstName">First Name:</label>
                <input type="text" value="" name="FirstName" id="FirstName">
            </li>
            <li>    
                <label for="LastName">Last Name:</label>
                <input type="text" value="" name="LastName" id="LastName">
            </li>
        </ul>
        <ul id="contact"> 
            <li> 
                <label for="PhoneNumber">Phone:</label>
                <input type="text" value="" name="PhoneNumber" id="PhoneNumber"><br>
                <small>including area code</small>
            </li>
            <li> 
                <label for="Email">Email:</label>
                <input type="text" value="" name="Email" id="Email">
                <input type="checkbox" value="true" name="SendCopy" id="SendCopy" checked="checked"><input type="hidden" value="false" name="SendCopy">
                <label class="send-copy" for="SendCopy">Send me a confirmation email</label>
            </li>
        </ul>   
    </fieldset>
    <fieldset id="CoverLetter">
        <legend>Cover Letter</legend>
        <ul>
            <li>
                <input type="radio" value="Upload" name="CoverLetterType" id="CoverLetterTypeUpload">
                <label for="CoverLetterTypeUpload">Upload one from your computer</label>
                <div id="UploadCoverLetter">
                <input type="file" name="CoverLetterUpload"><span id="CoverLetterUploadSize"></span>
                <small>Up to 2MB for file types .pdf .doc .docx .rtf .txt .html</small>

                
                    <div class="lightbox-trigger-content">
                    <input type="checkbox" value="true" name="StoreCoverLetter" id="StoreCoverLetter" class="lightbox-trigger"><input type="hidden" value="false" name="StoreCoverLetter">
                    <label for="StoreCoverLetter">Save to my account</label>
                    </div>                    
                

                </div>
            </li>
            <li>
                <input type="radio" value="Write" name="CoverLetterType" id="CoverLetterTypeWrite">
                <label class="grid_3" for="CoverLetterTypeWrite">Write one now <small class="grid_2 state-floatright" >or <a href="#" id="WriteCoverLetterClear">clear</a></small></label>             
                <div id="WriteCoverLetter">
                <textarea rows="12" name="CoverLetterText" id="CoverLetterText" cols="25"></textarea>
                </div>
            </li>
            <li>
                <input type="radio" value="None" name="CoverLetterType" id="CoverLetterTypeNone" checked="checked">
                <label for="CoverLetterTypeNone">Don't use one</label>
            </li>    
        </ul>
    </fieldset>


    <fieldset id="Resume">
        <legend class="grid_5">Resume<a class="state-floatright" href="#_" id="ProtectPrivacyTips">Protect your privacy</a></legend>        
        <div style="display:none" class="apply-tips protectprivacy"> 
            <p>For your security, we recommend you don't include your street address, date of birth, bank account details or a photo in your resume.</p>

        </div>
		<br />
        <ul>
            <li>
                <input type="radio" value="Upload" name="ResumeType" id="ResumeTypeUpload" checked="checked">
                <label for="ResumeTypeUpload">Upload one from your computer</label>
                <div id="UploadResume">
                <input type="file" name="ResumeUpload"><span id="ResumeUploadSize"></span>
                
                <small>Up to 2MB for file types .pdf .doc .docx .rtf .txt .html</small> 
                    <div class="lightbox-trigger-content">
                <input type="checkbox" value="true" name="StoreResume" id="StoreResumeCheckbox" class="lightbox-trigger"><input type="hidden" value="false" name="StoreResume">
                    <label for="StoreResume">Store on my account</label>
                    
                </div>
                </div>
            </li>
                
                <li class="lightbox-trigger-content">
                <input type="radio" value="Stored" name="ResumeType" id="ResumeTypeStored" class="lightbox-trigger">
                
                     <label for="ResumeTypeStored">Choose one from your account</label>
                
                
                <ul style="display:none" data-bind="template: { name: 'resumeListTemplate', foreach: resumes }" id="StoredResume">
                
                </ul>
                <script id="resumeListTemplate" type="text/x-jquery-tmpl">
                    &lt;li&gt;
                        {{if selected}}
                            &lt;input type="radio" value="${id}" name="ResumeId" checked="checked"&gt;
                        {{else}}
                            &lt;input type="radio" value="${id}" name="ResumeId"&gt;
                        {{/if}}
                        &lt;label&gt;
                            &lt;a href="/MyAccount/ViewResume?id=${id}"&gt;${name}&lt;/a&gt;
                            &lt;small&gt;added ${uploadedDate}&lt;/small&gt;
                        &lt;/label&gt;
                    &lt;/li&gt;
                </script>
                
                </li>
            <li>
                <input type="radio" value="None" name="ResumeType" id="ResumeTypeNone">
                <label for="ResumeTypeNone">Don't use one</label>
            </li>
        </ul>  
    </fieldset>



    <p class="form-submit">
        <input type="submit" class="btn state-btn-large state-inline state-btn-cta in-callout apply-btn" value="Submit Application" id="SubmitLink">        
    </p>
    
    
    
    <small>
        When you submit this application, we'll only pass the personal information you've just
        given us onto the company advertising this job (More Jobs Online). That's all.<br>
        We're always careful with <a href="/privacy" class="privacy">your privacy</a>.
    </small> 
    <input type="hidden" value="22588962" name="JobId" id="JobId">
    <input type="hidden" value="False" name="IsEoi" id="IsEoi">
    <input type="hidden" value="False" name="IsAdvertiserEoi" id="IsAdvertiserEoi">
    <input type="hidden" value="" name="AdvertiserIds" id="AdvertiserIds">
    <input type="hidden" value="False" name="SelectionCriteriaEnabled" id="SelectionCriteriaEnabled">
    <input type="hidden" value="" name="ScreenData" id="ScreenData">
</form>
<?php endif; ?>
</div>

<div class="grid_6 l-column content-preview">
    <div class="mod-job-details">
        <ul>        
            <li>
                <label>Work type:</label>
                <div><?=$this->job->employmenttype?></div>
            </li>            
            <li>
                <label>Classification:</label>
                <div><?=$this->job->classification?></div>
            </li>
        </ul>
    </div>
	
	<?php if($this->job->applicationurl==''): ?>
    <br />
    <div id="AdDetailsDiv" style="border: 1px solid #000;">
        <div align="left" class="pt1_border">
		  <div class="pt1_header" style="border-bottom: 1px solid #000;">
			<div align="right"><img width="255" vspace="1" hspace="1" height="45" alt="<?=$this->job->position?>" src="images/recruiter_84.png"></div>
			<div class="jobtitle pt1_jt">
				&nbsp;
			</div>
		  </div>
		  <div class="pt1_text" style="padding:1em">
			<div class="templatetext">
				<?=$this->job->description?>
			</div>
			<br />
			<div class="details">
				&nbsp;
			</div>
		  </div>
	  <div class="pt1_baseblue">
		<div class="pt1_basegrey">-</div>
	  </div>
	</div>
    </div>
    <?php endif; ?>
</div>
<?php if($this->job->applicationurl!=''): ?>
	<iframe src="<?=$this->job->applicationurl?>" width="100%" scrolling="no" height="700" frameborder="0" align="middle" cellpadding="0" cellspacing="0" border="0"></iframe>
<?php endif; ?>
</div>
<div class="mod-section state-section-page l-clearfix l-row" style="min-height:0px;">
	<a href="#" class="goback">
	<span class="ico ico-18 ico-magnify"></span>
	back to results
	</a>
	<center>
		<div class="grid_7 l-column state-searchresults-tools" style="float:none;text-align:left;width:auto;">
			<div class="mod-callout state-socialshare">       
		   		<ul class="socialicons color">
					<li><a href="share.php?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('share.php?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&title=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=600,height=700');return false;" class="email" target="_blank" title="Email a friend">email</a></li>
					<li><a class="facebook" target="_blank" title="share this job on Facebook" href="http://www.facebook.com/share.php?u=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&p[title]=<?=urlencode($this->job->position)?>&p[summary]=<?=urlencode(strip_tags($this->job->description))?>&p[images][0]=<?=Controller::host()?>/images/apply.jpg','sharer','toolbar=0,status=0,width=620,height=436');return false;">facebook</a></li>
					<li><a class="twitter" target="_blank"  title="share this job on Twitter" href="https://twitter.com/share?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('https://twitter.com/share?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&text=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=620,height=436');return false;">twitter</a></li>
					<li><a class="linkedin" target="_blank"  title="share this job on LinkedIn" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&title=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=620,height=436');return false;">linkedin</a></li>
					<li><a class="gpluslight" target="_blank"  title="share this job on Google+" href="https://plus.google.com/share?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('https://plus.google.com/share?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>'),'sharer','toolbar=0,status=0,width=620,height=436');return false;">gpluslight</a></li>
			  </ul>
			</div>
		</div>
	</center>
</div>
<div class="mod-search-form l-row is-collapsed state-mini-search" id="bottomsearchBoxContainer"></div>
    <!--Leaderboard banner-->    
<div class="mod-section l-row mod-leaderboard-banner" id="leaderboard"> 		
	<iframe src="ads.php/2/<?=$this->searchlist?> jobs <?=$this->searchBy?> in <?=$this->location?>" scrolling="no" height="100" width="738" style="border:none;"></iframe>
</div>
<script>
$('#bottomsearchBoxContainer').html($('#searchBoxContainer').clone().html());
$('#searchBoxContainer select').each(function(k,v){
	$('#bottomsearchBoxContainer #'+v.id).val($('#searchBoxContainer #'+v.id).val());
});
$('#bottomsearchBoxContainer #Keywords').val($('#searchBoxContainer #Keywords').val());
$('.goback').click(function(event){
	var param = 'goback=true';
	$('#content').load('engine/jobs.php',param);
	$('html, body').animate({scrollTop:0}, 'slow');
	event.preventDefault();
});
</script>	
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12247273-10']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>