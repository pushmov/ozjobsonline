<?php
require_once('controller.php');
class Location extends Controller
{
	public function display()
	{
		if(isset($_POST['CityID']))
		{
			$CityID = $_POST['CityID'];
			$this->Options = $this->model->getAreaOptions($CityID);
		}
		if(isset($_POST['countryid']))
		{
			$countryid = $_POST['countryid'];
			$this->Options = $this->model->getLocationOptions($countryid);
		}
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();