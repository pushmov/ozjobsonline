<?php
require_once('model.php');
class AddetailModel extends Model
{
	public function getJob($id)
	{
		$sql = "SELECT `PrimaryKey` as `id`,`job`, `job_action`, `advertisername`, `advertisertype`, `advertisertype_valueid`, `senderreference`, `displayreference`, `classification`, `classification_valueid`, `position`, `description`, `country`, `country_valueid`, `location`, `location_valueid`, `area`, `area_valueid`, `postalcode`, `applicationurl`, `descriptionurl`, `language`, `language_valueid`, `contactname`, `employmenttype`, `employmenttype_valueid`, `startdate`, `duration`, `workhours`, `workhours_valueid`, `salarycurrency`, `salarycurrency_valueid`, `salaryminimum`, `salarymaximum`, `salaryperiod`, `salaryperiod_valueid`, `salaryadditional`, `jobsource`, `jobsourceurl`, `videolinkurl` FROM `xml_jobg8_oz` WHERE `PrimaryKey`=%d";
		$sql = sprintf($sql,$id);
		return $this->queryObject($sql);	
	}
}