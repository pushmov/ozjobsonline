<?php
require_once('model.php');
class LatestrecruiterModel extends Model
{	
	public function getLatestrecruiter()
	{
		$sql = "SELECT 
					a.`PrimaryKey` , 
					a.`position` , 
					a.`description` 
				FROM 
					`xml_jobg8_oz` a 
				WHERE a.`advertisertype_valueid` IN (15890,15891) LIMIT 50 ";
		return $this->query($sql);
	}	
}