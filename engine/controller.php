<?php
 
abstract class Controller
{	
	public $model = null;		
	public function __construct()	
	{		
		require_once(strtolower(get_class($this)).'.model.php');		
		$model = get_class($this).'Model';		
		$this->model = new $model;	
	}		
	
	public function display()	
	{		
		require_once(strtolower(get_class($this)).'.view.php');	
	}

	public static function host()
	{
		$host = explode('/',ltrim($_SERVER["REQUEST_URI"],'/')); array_pop($host); $host = '/'.implode('/',$host);
		if(strpos($host,'/engine')!==false)
			$host = substr($host,0,-7);
		$host = 'http://'.$_SERVER['HTTP_HOST'].$host;
		return $host;
	}
}