<?php
require_once('controller.php');
class Latestrecruiter extends Controller
{
	public function display()
	{
		$this->Latestrecruiter = $this->model->getLatestrecruiter();
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();