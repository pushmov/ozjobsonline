<?php
require_once('controller.php');
class Apply extends Controller
{
	public function display()
	{		
		$id = $_GET['target'];
		$this->job = $this->model->getJob($id);
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();