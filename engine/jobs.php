<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once('controller.php');
require_once('search.model.php');
class Jobs extends Controller
{
	public function display()
	{				
		if(count($_GET)>0)
		{
			$this->loadPost();
			if(array_key_exists('worktype',$_GET))
			{
				$_POST['worktype']=$_GET['worktype'];
				$_POST['worktype_str']=$_GET['txt'];
			}
			if(array_key_exists('arrworktype',$_GET))
			{
				$arr = json_decode($_GET['arrworktype']);
				if(is_array($arr) && count($arr)>0)
					$_POST['arrworktype']="'".implode("','",$arr)."'";
			}
			if(array_key_exists('page',$_GET))
			{
				$_POST['page']=$_GET['page'];
			}
			$this->cachePost();
		}
		$searchModel = new SearchModel();
		$this->model->Prepare();
		$this->pagination = $this->model->getPagination();
		$this->jobs = $this->model->getJobs();
		$this->searchlist = '';
		if($_POST['worktype']!=0)
			$this->searchlist .= $_POST['worktype_str'];
		if($_POST['industry']!=0)
		{
			$this->searchlist .= ' '.$_POST['industry_str'];		
		}
		if($_POST['occupation']!='0')
		{
				$this->searchlist .= ' '.$_POST['occupation'];				
		}
		$this->searchBy = '';
		if($_POST['recruiters']!='0' || $_POST['employers']!='0' || array_key_exists('advertiser',$_POST))
		{
			$searchBy = array();
			if($_POST['recruiters']!='0')
				$searchBy[] = $_POST['recruiters'];
			if($_POST['employers']!='0')
				$searchBy[] = $_POST['employers'];
			if(array_key_exists('advertiser',$_POST))
				$searchBy[] = $_POST['advertiser'];
			$this->searchBy = ' by <strong>'.implode(', ',$searchBy).'</strong>';
		}
		
		$this->location = 'All Locations';
		if($_POST['parentlocation']!=0)
		{
			if(array_key_exists('childlocation',$_POST) && $_POST['childlocation']!=0)
				$this->location = $_POST['childlocation_str'].' '.$_POST['parentlocation_str'];
			else
				$this->location = $_POST['parentlocation_str'];
		}				
		//$this->WorkTypeOptions = $searchModel->getWorkTypeOptions();
		$this->workTypeList = $this->model->WorkTypeList();
		$this->industryList = $this->model->ClassificationList();
		if(count($this->industryList)<=1)
			$this->occupationList = $this->model->OccupationList();
		$this->locationList = $this->model->LocationList();
		if(count($this->locationList)<=1)
			$this->areaList = $this->model->AreaList();		
		parent::display();
	}
	
	private function loadPost()
	{
		if(get_magic_quotes_gpc ())
			$_COOKIE['csearch'] = stripslashes ( $_COOKIE['csearch'] );
		$cookie = json_decode($_COOKIE['csearch']);
		if(is_array($cookie))
			foreach($cookie as $v)
				$_POST[$v->name] = $v->value;
	}
	private function cachePost()
	{
		$cookie = array();
		foreach($_POST as $k=>$v)
			$cookie[] = '{"name":"'.$k.'","value":"'.$v.'"}';
		setcookie('csearch','['.implode(',',$cookie).']',0,'/');
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();