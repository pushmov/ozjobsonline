<?php
require_once('controller.php');
class Latestemployer extends Controller
{
	public function display()
	{
		$this->Latestemployer = $this->model->getLatestemployer();
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();