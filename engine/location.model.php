<?php
require_once('model.php');
class LocationModel extends Model
{	
	public function getAreaOptions($CityID)
	{
		if($CityID>0)
		{
			$params = array();
			$sql = "SELECT DISTINCT `area` AS SubCity, `area_valueid` AS SubCityID FROM `xml_jobg8_oz` WHERE `location_valueid`=:location ORDER BY `area`";
			$params[':location'] = $CityID;
			return $this->query($sql,$params);		
		}
		else
			return array();
	}	
	public function getLocationOptions($countryid)
	{		
		$params = array();
		$sql = "SELECT DISTINCT `location_valueid`, `location` FROM `xml_jobg8_oz`";
		if($countryid>0)
		{
			$sql .= " WHERE `country_valueid`=:countryid ORDER BY `country`,`location`";
			$params[':countryid'] = $countryid;
		}
		else
			$sql .= " ORDER BY `location`";
		return $this->query($sql,$params);		
	}
}