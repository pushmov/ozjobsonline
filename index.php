<?php include('./engine/config.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="site.css" rel="stylesheet" type="text/css" />
<link href="site2.css" rel="stylesheet" type="text/css" />
<link href="images/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/favicon.ico">
<link rel="alternate" type="application/rss+xml" title="ICT Solutions and ICT Services RSS Feed" href="http://www.globologic.com/rss/rss.xml" />
<title>Find job in Australia. Find Aussie work. Search for a job online at Oz Jobs Online</title>
<meta name="keywords" content="work, job, australia, jobs online, australian job, oz jobs, oz, aussie jobs, aussie">
<meta name="description" content="Looking for work in Australia, find a Aussie job by searching online at Oz Jobs Online">
<meta property="og:description" content="Find work in Australia with a job from Oz Jobs Online">
<meta property="og:site_name" content="ozjobsonline.ocm">
<meta name="dcterms.rightsHolder" content="Globo Logic Pty Ltd">
<meta name="dcterms.dateCopyrighted" content="21-03-2013">
<meta name="robots" content="index, follow">
<meta name="robots" content="noodp">
<meta property="dc.language" content="english">
<meta property="dc.source" content="http://www.ozjobsonline.com/">
<meta property="dc.relation" content="http://www.morejobsonline.com/">
<meta property="dc.title" content="Looking for a job in Australia. Find Aussie work online at Oz Jobs Online">
<meta property="dc.keywords" content="jobs, work, oz jobs, australian jobs, aussie, jobs online, work in australia, aussie">
    <script language="JavaScript" type="text/javascript" src="jquery-1.7.2.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="jquery.cookie.js"></script>
	<script language="JavaScript" type="text/javascript" src="json.js"></script>
    
	<script language="JavaScript" type="text/javascript">
		(function($){
			$(document).ready(function(){
				$('#shortcut').siblings().filter('img').css('cursor','pointer').click(function(){location.reload();});
				runner();
			});			
		})(jQuery);
	</script>
	   <script language="JavaScript" type="text/javascript">
        // JavaScript to interpolate random images into a page. 
        var ic = 7; // Number of alternative images 
        var xoxo = new Array(ic);  // Array to hold filenames 

        xoxo[0] = "images/banner3.jpg";
        xoxo[1] = "images/banner4.jpg";
        xoxo[2] = "images/banner5.jpg";
        xoxo[3] = "images/banner9.jpg";
        xoxo[4] = "images/banner12.jpg";
        xoxo[5] = "images/banner13.jpg";
        xoxo[6] = "images/banner14.jpg";

        function pickRandom(range) {
            var now = new Date();
            return now.getSeconds() % range;
        }
        // Write out an IMG tag, using a randomly-chosen image name. 
        var choice = pickRandom(ic);


        function ShowPopup() {
//            var iHeight
//            if (screen.height > document.body.offsetHeight) {
//                iHeight = screen.height
//            } else {
//                iHeight = document.body.offsetHeight + 20;
//            }
//            document.getElementById('divPopupMask').style.height = iHeight + 'px';
//            document.getElementById('divPopupImage').style.top = Math.round((document.documentElement.clientHeight / 2) + document.documentElement.scrollTop) - 150 + 'px';

//            document.getElementById('divPopup').style.display = 'block';
        }

        function HidePop() {
            document.getElementById('divPopup').style.display = 'none';
        }
		
		function runner()
		{	
			setTimeout("runner()",3000);
			scrolling();
		}
		function scrolling()
		{
			$('.scrolling').each(function(){
				var s = $(this);
				var c = $('li', s).length;
				if(c>3)
				{
				if($('li:hidden', s).length)
				{
					$('li:hidden', s).last().next().fadeOut("slow");
					if($('li:hidden', s).length>c-6)
					{
						var cp = $('li:hidden', s).first().clone();
						$('li:hidden', s).first().remove();
						cp.appendTo(s).fadeIn('slow');
					}
				}
				else
				{
					$('li', s).first().fadeOut("slow");
				}
				}
				
			});
		}
    </script>
    <style type="text/css">
        .modalBackground {
	        background-color: Gray;
	        filter: alpha(opacity=70);
	        opacity: 0.7;
        }
    </style>
		<?php if(ENVIRONMENT != 'DEVELOPMENT') : ?>
		<script language=JavaScript> var message="Our website is Copyrighted. Trying to see our source or steal images is illegal"; function clickIE4(){ if (event.button==2){ alert(message); return false; } } function clickNS4(e){ if (document.layers||document.getElementById&&!document.all){ if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false") </script>
		<?php endif; ?>
</head>
<body class="FourColFixHdr" onload="ShowPopup();">    
    <div id="container">
        <div id="header">
            <table width="100%">
                <tr>
                    <td align="left" style="padding: 30px 0px 10px 0px;">
						<div id="shortcut">
							<div class="mod-field">
							 <?php require_once('more_lists.html') ;?>							
							</div>
							</div>
                        <img src="images/logo.jpg" /> Find work in Australia with a job by Oz Jobs Online | 
						<a href="about.php" title="About us"> About us </a> | 
						<a href="contact.php" title="Contact us">Contact us </a>| 
						<a href="http://www.morejobsonline.com/postjob.html" target="_blank" title="Post a job for only $50">Post a Job </a>
                    </td>
                </tr>
            </table>
            <div style="width:1020px;" id="searchjobs">		
				<?php require_once('./engine/search.php') ;?>
			</div>
        </div>
		<div id="content">
        <!-- ************************** START COLUMN ONE Latest Jobs  ****************************** -->
        <div id="column1">
            <h3>LATEST JOBS</h3>
            <p>
                <a href="">
                    <img src="images/latest_jobs.jpg" width="220" height="110" border="0" style="margin-top: 6px;
                        margin-bottom: 15px; border-style: solid; border-color: #999" /></a></p>    
			<?php require_once('./engine/latestjobs.php') ;?>
            <!-- end #column1 -->
        </div>
        <!-- ********************* START COLUMN TWO Latest Recruiters  ***************************** -->
        <div id="column2">
            <h3>
                LATEST RECRUITER JOBS</h3>
            <p>
                <a href="">
                    <img src="images/recruiting_jobs.jpg" width="220" height="110" border="0" style="margin-top: 6px;
                        margin-bottom: 15px; border-style: solid; border-color: #999" /></a></p>   
			<?php require_once('./engine/latestrecruiter.php') ;?>
            <!-- end #column2 -->
        </div>
        <!-- *********************** START COLUMN THREE Latest Employers  ************************* -->
        <div id="column3">
            <h3>
                LATEST EMPLOYER JOBS</h3>
            <p>
                <a href="">
                    <img src="images/employers_jobs.jpg" width="220" height="110" border="0" style="margin-top: 10px;
                        margin-bottom: 15px; border-style: solid; border-color: #999" /></a></p>
			<?php require_once('./engine/latestemployer.php') ;?>
            <!-- end #column3 -->
        </div>
        <!-- ************************* START COLUMN FOUR Post a Job  **************************** -->
        <div id="column4">
            <h3>
                POST A JOB</h3>
            <p>
                <a href="http://www.morejobsonline.com/postjob.html" title="Post a job ad for only $50" target="_blank">
                    <img src="images/job_posta_logo.jpg" width="220" height="110" border="0" style="margin-top: 6px;
                        margin-bottom: 15px; border-style: solid; border-color: #999" /></a></p>
            <div id="itemheading">
                About OzJobsOnline</div>
            <div id="newsitem">
                If your looking for a job in Australia? then Oz Jobs Online is the number one place for you to find 
                all your Australian jobs. A part of the More Jobs Online group of sites with over 50 job boards.
                <div id="more">
                    <!--<a href="">Read more</a>--></div>
            </div>
            <div id="itemheading">
                Job Posta</div>
            <div id="newsitem">
               Recruiters and Companies can post jobs to multiple job boards easier and cheaper than any other 3rd 
               party auto posting solution. If you want to post a job to our job boards it's only $50.00 each ad
               and they appear on all of our sites
                <div id="more">
                    <a href="http://www.jobposta.com" title="Auto Job Posting solution" target="_blank">Read more</a></div>
            </div>
			
			<div id="itemheading">
                Only $50</div>
            <div id="newsitem">
               Placing an ad on our sites only costs $50.00 per job ad. Your ad doesn't expire either - ad remains until either position is filled and/or you remove it.
                <div id="more">
                     <a href="http://www.morejobsonline.com/postjob.html" title="Post a job ad for only $50" target="_blank">Post Job</a></div>
            </div>
            <!-- end #column4 -->
        </div>
		
		
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="right" style="background-color: #FFF; margin-top: 10px;">
                    
                </td>
            </tr>
        </table>
        
        <!-- end #mainContent -->
		<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats -->
		<br class="clearfloat" />		
		<?php require_once('footer.html') ;?> 
    <!-- end #container -->
   
</body>
</html>