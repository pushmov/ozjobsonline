<?php
require_once('controller.php');
class Classification extends Controller
{
	public function display()
	{
		$Classification1ID = $_POST['Classification1ID'];
		$this->SubClassificationOptions = $this->model->getSubClassificationOptions($Classification1ID);
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();