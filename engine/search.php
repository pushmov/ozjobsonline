<?php
require_once('controller.php');
class Search extends Controller
{
	public function display()
	{
		$this->ClassificationOptions = $this->model->getClassificationOptions();
		$this->SubClassificationOptions = $this->model->getSubClassificationOptions();
		$this->LocationOptions = $this->model->getLocationOptions();
		$this->WorkTypeOptions = $this->model->getWorkTypeOptions();
		$this->RecruitersOptions = $this->model->getRecruitersOptions();
		$this->EmployersOptions = $this->model->getEmployersOptions();
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();