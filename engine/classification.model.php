<?php
require_once('model.php');
class ClassificationModel extends Model
{	
	public function getSubClassificationOptions($Classification1ID)
	{
		$sql = '';
		$params = array();
		if($Classification1ID>0)
		{
			$sql = "SELECT DISTINCT `position` AS Classification2ID , `position` AS Classification2Name FROM `xml_jobg8_oz` WHERE classification_valueid=:classification ORDER BY `position`";
			$params[':classification'] = $Classification1ID;
		}
		else
			$sql = "SELECT DISTINCT `position` AS Classification2ID , `position` AS Classification2Name FROM `xml_jobg8_oz` ORDER BY `position`";
		return $this->query($sql,$params);		
	}	
}