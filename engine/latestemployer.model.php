<?php
require_once('model.php');
class LatestemployerModel extends Model
{	
	public function getLatestemployer()
	{
		$sql = "SELECT 
					a.`PrimaryKey` , 
					a.`position` , 
					a.`description` 
				FROM 
					`xml_jobg8_oz` a
				WHERE a.`advertisertype_valueid` IN (15892) LIMIT 50 ";
		return $this->query($sql);
	}	
}