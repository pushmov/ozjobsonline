<div class="joblisting-index mod-section state-section-page l-clearfix l-row">          
<div class="state-alignright state-bottom-border">
		<span class="state-floatleft advertiserLabel">Advertiser:&nbsp;</span><span class="state-floatleft advertiserText"><?=$this->job->advertisername?></span>
		<span class="state-alignright moreAdvertiser"><a href="#" target="<?=$this->job->advertisername?>">More jobs by this advertiser</a></span>
    <i class="ico ico-23 ico-magnify"></i><a href="#" class="goback">back to results</a>
</div>
<div class="state-floatleft grid_6 l-column">
	<div style="float:left;">
    <div class="mod-job-details">
        <ul>
		<li>
                <label>Country:</label>
                <div><?=$this->job->country?></div>
            </li> 
            <li>
                <label>Location:</label>
                <div>
				<?=$this->job->location?><span class="mod-arrow state-arrow-right"></span><?=$this->job->area?>
            </div></li>            
            <li>
                <label>Work type:</label>
                <div><?=$this->job->employmenttype?></div>
            </li>            
		<li>
                <label>Advertiser:</label>
                <div>
				<?=$this->job->advertisername?>
                </div>
            </li>
			<?php				
				$symbol = '';
				$currency = explode('.',$this->job->salarycurrency);
				if(count($currency)>1)
					$symbol = $currency[1];
				$start = $this->job->startdate;
				$startarr = explode('T',$this->job->startdate);
				if(count($startarr)>1)
					$start = $startarr[0];
				
			?>
			<?php if(isset($this->job->salaryminimum) && $this->job->salaryminimum!=''):?>
				<li>
					<label>Salary Min:</label>
					<div>
						<?php echo $symbol.' '.number_format($this->job->salaryminimum);?>
					</div>
				</li>
			<?php endif;?>
			<?php if(isset($this->job->salarymaximum) && $this->job->salarymaximum!=''):?>
				<li>
					<label>Salary Max:</label>
					<div><?php echo $symbol.' '.number_format($this->job->salarymaximum);?></div>
				</li>
			<?php endif;?>
			<?php if(isset($this->job->salaryadditional) && $this->job->salaryadditional!=''):?>
				<li>
					<label>Salary:</label>
					<div>
						<?php echo $this->job->salaryadditional;?>
					</div>
				</li>
			<?php endif;?>
			<?php if(isset($start) && $start!=''):?>
				<li>
					<label>Start:</label>
					<div>
						<?php echo $start;?>
					</div>
				</li>
			<?php endif;?>
			<?php if(isset($this->job->duration) && $this->job->duration!=''):?>
				<li>
					<label>Duration:</label>
					<div>
						<?php echo $this->job->duration;?>
					</div>
				</li>
			<?php endif;?>
        </ul>
    </div>

    <div class="mod-section">        
		<div class="state-sideapply state-floatleft grid_3">    
			<a href="#" class="state-inline applynow" target="<?=$this->job->id?>"><img src="images/apply.jpg" alt="Apply Now"/></a>	
		</div>
		<div class="state-clear"></div>
    </div>
    
    <div class="mod-section mod-callout state-socialshare grid_4">            
			  <ul class="socialicons color">
					<li><a href="share.php?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('share.php?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&title=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=600,height=700');return false;" class="email" target="_blank" title="Email a friend">email</a></li>
					<li><a class="facebook" target="_blank" title="share this job on Facebook" href="http://www.facebook.com/share.php?u=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&p[title]=<?=urlencode($this->job->position)?>&p[summary]=<?=urlencode(strip_tags($this->job->description))?>&p[images][0]=<?=Controller::host()?>/images/apply.jpg','sharer','toolbar=0,status=0,width=620,height=436');return false;">facebook</a></li>
					<li><a class="twitter" target="_blank"  title="share this job on Twitter" href="https://twitter.com/share?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('https://twitter.com/share?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&text=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=620,height=436');return false;">twitter</a></li>
					<li><a class="linkedin" target="_blank"  title="share this job on LinkedIn" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&title=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=620,height=436');return false;">linkedin</a></li>
					<li><a class="gpluslight" target="_blank"  title="share this job on Google+" href="https://plus.google.com/share?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('https://plus.google.com/share?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>'),'sharer','toolbar=0,status=0,width=620,height=436');return false;">gpluslight</a></li>
			  </ul>
        </div>
    </div>

</div>
<div></div>
<div class="grid_6 state-job-ad">
<div>
    <h1 class="state-job-title"><?=$this->job->position?></h1>
    
</div>
<br>    
<div>
	<div align="left" class="pt1_border">
	  <div class="pt1_header">
		<div align="left">&nbsp;</div>
	  </div>
	  <div class="pt1_text">
		<div class="templatetext">
			<iframe id="description" style="width:100%;border:none;"></iframe>	
		</div>	   
	  </div>
	</div>
</div>
    
    <!-- START: Apply section below template -->
	<div class="mod-section">       
		<div id="rightapply" class="state-baseapply state-floatleft grid_3">		
			<a href="#" class="state-inline applynow" target="<?=$this->job->id?>"><img src="images/apply.jpg" alt="Apply Now"/></a>		
		</div>
	</div>
</div>
</div>
<div class="mod-section state-section-page l-clearfix l-row" style="min-height:0px;">
	<a href="#" class="goback">
	<span class="ico ico-18 ico-magnify"></span>
	back to results
	</a>
	<center>
		<div class="grid_7 l-column state-searchresults-tools" style="float:none;text-align:left;width:auto">
			<div class="mod-callout state-socialshare">       
		   		<ul class="socialicons color">
					<li><a href="share.php?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('share.php?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&title=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=600,height=700');return false;" class="email" target="_blank" title="Email a friend">email</a></li>
					<li><a class="facebook" target="_blank" title="share this job on Facebook" href="http://www.facebook.com/share.php?u=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&p[title]=<?=urlencode($this->job->position)?>&p[summary]=<?=urlencode(strip_tags($this->job->description))?>&p[images][0]=<?=Controller::host()?>/images/apply.jpg','sharer','toolbar=0,status=0,width=620,height=436');return false;">facebook</a></li>
					<li><a class="twitter" target="_blank"  title="share this job on Twitter" href="https://twitter.com/share?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('https://twitter.com/share?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&text=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=620,height=436');return false;">twitter</a></li>
					<li><a class="linkedin" target="_blank"  title="share this job on LinkedIn" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>')+'&title=<?=$this->job->position?>','sharer','toolbar=0,status=0,width=620,height=436');return false;">linkedin</a></li>
					<li><a class="gpluslight" target="_blank"  title="share this job on LinkedIn" href="https://plus.google.com/share?url=<?php echo urlencode(Controller::host().'/detail.php?job='.base64_encode($this->job->senderreference));?>" onclick="javascript:window.open('https://plus.google.com/share?url='+encodeURIComponent('<?=Controller::host()?>/detail.php?job=<?=base64_encode($this->job->senderreference)?>'),'sharer','toolbar=0,status=0,width=620,height=436');return false;">gpluslight</a></li>
			  </ul>
			</div>
		</div>
	</center>
</div>		
<div class="mod-search-form l-row is-collapsed state-mini-search" id="bottomsearchBoxContainer"></div>
    <!--Leaderboard banner-->    
<div class="mod-section l-row mod-leaderboard-banner" id="leaderboard"> 		
	<iframe src="ads.php/2/<?=$this->searchlist?> jobs <?=$this->searchBy?> in <?=$this->location?>" scrolling="no" height="100" width="738" style="border:none;"></iframe>
</div>
<script type="text/javascript">
$('#description').ready(function(){
var description = '<?=addslashes($this->job->description)?>';
var ifrm = document.getElementById('description');
ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
ifrm.document.open();
ifrm.document.write(description);
ifrm.document.close();
$('#description').height($('#description').contents().find('body').outerHeight()+35);
});
$('#bottomsearchBoxContainer').html($('#searchBoxContainer').clone().html());
$('#searchBoxContainer select').each(function(k,v){
	$('#bottomsearchBoxContainer #'+v.id).val($('#searchBoxContainer #'+v.id).val());
});
$('#bottomsearchBoxContainer #Keywords').val($('#searchBoxContainer #Keywords').val());
$('.applynow').click(function(event){
	var param = 'target='+$(this).prop('target');
	$('#content').load('engine/apply.php',param);
	event.preventDefault();
});
$('.goback').click(function(event){
	var param = 'goback=true';
	$('#content').load('engine/jobs.php',param);
	$('html, body').animate({scrollTop:0}, 'slow');	
	event.preventDefault();
});
if($('.pt1_border').height() < screen.height)
	$('#rightapply').hide();
</script>		
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12247273-10']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>