<?php
require_once('controller.php');
class Addetail extends Controller
{
	public function display()
	{		
		$id = $_GET['target'];
		$this->job = $this->model->getJob($id);
		if($this->job===false)
		{
			$this->job = new stdClass();
			$this->job->description = "JOB NO LONGER AVAILABLE";
		}
		parent::display();
	}
}
$class = ucfirst(current(explode('.',basename(__FILE__))));
$tmpl = new $class();
$tmpl->display();