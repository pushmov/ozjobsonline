<form id="seachForm">
<div class="mod-search-form l-row" id="searchBoxContainer">
	<div class="l-column grid_4">
		<div class="mod-field">
			<label for="catindustry" id="classLabel">Job Sector</label>
			<select name="industry" id="catindustry" class="resizeable populatehidden" title="industry_str">
			   <option value="0">Any Job Sector</option>
			   <?php foreach($this->ClassificationOptions as $option) : ?>
					<option value="<?=$option->Classification1ID?>"><?=$option->Classification1Name?></option>
			   <?php endforeach; ?>
			</select>
			<input type="hidden" name="industry_str" value="" />
		</div>
		<div class="mod-field"> 
			<label for="catoccupation" id="subclassLabel">Job Title</label>
			<select name="occupation" id="catoccupation" class="resizeable">
				<option value="0">Any Job Title</option>
				<?php foreach($this->SubClassificationOptions as $option) : ?>
					<option value="<?=$option->Classification2ID?>"><?=$option->Classification2Name?></option>
			   <?php endforeach; ?>
			</select>
		</div>	
		<div class="mod-field">
			<label for="recruiters" id="recruitersLabel">Recruiters</label>
			<select name="recruiters" id="recruiters" class="resizeable">
				<option value="0">All Recruiters</option>
				<?php foreach($this->RecruitersOptions as $option) : ?>
					<option value="<?=$option->advertisername?>"><?=$option->advertisername?></option>
			   <?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="l-column grid_4">
		<div class="mod-field">
			<label for="catparentlocation" id="loclabel">Area</label>
			<select name="parentlocation" id="catparentlocation" class="resizeable populatehidden" title="parentlocation_str">
				<option value="0">All Areas</option>
			<?php foreach($this->LocationOptions as $location) : ?>
					<option value="<?=$location->CityID?>"><?=$location->City?></option>
			<?php endforeach; ?>	
			</select>
			<input type="hidden" name="parentlocation_str" value="" />
		</div>
		<div class="mod-field mod-area">       
			<label for="catchildlocation" id="arealabel">Location</label>
			<select name="childlocation" id="catchildlocation" disabled="disabled"  class="resizeable populatehidden" title="childlocation_str">
				<option value="0">All Locations</option></select>
				<input type="hidden" name="childlocation_str" value="" />
			<label for="chkUnspecifiedchildlocation" class="mod-area-input is-hidden" id="unspecLabel"><input type="checkbox" name="JobSearchBox:chkUnspecifiedchildlocation" class="unspecArea" id="chkUnspecifiedchildlocation" disabled=""> Include job ads not specifying an area</label>
		</div>		
		<div class="mod-field">
			<label for="employers" id="employersLabel">Employers</label>
			<select name="employers" id="employers" class="resizeable">
			   <option value="0">Any Employers</option>
			   <?php foreach($this->EmployersOptions as $option) : ?>
					<option value="<?=$option->advertisername?>"><?=$option->advertisername?></option>
			   <?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="l-column grid_4">			
		<div class="mod-field">
			<label for="Keywords" id="keywordLabel">Keywords</label>
			<input type="search" placeholder="Search by Keywords" name="Keywords" id="Keywords" class="enter-text hasPlaceholder">
		</div>
		<div class="mod-field">
			<label for="catworktype" id="workLabel">Work Type</label>
			<select name="worktype" id="catworktype" class="resizeable populatehidden" title="worktype_str">
			<option value="0" selected="selected">All Work Types</option>
			<?php foreach($this->WorkTypeOptions as $option) : ?>
				<option value="<?=$option->employmenttype_valueid?>"><?=$option->employmenttype?></option>
		   <?php endforeach; ?>
			</select>
			<input type="hidden" name="worktype_str" value="" />
		</div>
		<div class="mod-submit-search l-clearfix">
			<input type="submit" class="btn state-btn-large state-btn-cta" value="SEARCH" name="DoSearch" id="DoSearch"><br />			
		</div>
	</div>	
</div>
</script>
<script language="JavaScript" type="text/javascript">
	(function($){
		$('#catindustry').change(function(){
			$.ajax({
				url:'engine/classification.php',
				data:'Classification1ID='+$(this).val(),
				dataType:'json',
				type:'POST'
			}).done(function(result){
				var opt = $('#searchBoxContainer #catoccupation option:nth-child(1)').clone();
				$('#catoccupation').html('').append(opt);
				$.each(result,function(i,d){
					$('#catoccupation').append('<option value="'+d['Classification2ID']+'">'+d['Classification2Name']+'</option>');
				});
				if($('#bottomsearchBoxContainer #catoccupation').length)
				{
					$('#bottomsearchBoxContainer #catoccupation').html($('#catoccupation').clone().html());
				}
			});
		});
		$('select.populatehidden').change(function(){
			if($(this).val()==0)
				$('input[name="'+$(this).prop('title')+'"]').val('');
			else
				$('input[name="'+$(this).prop('title')+'"]').val($('option:selected',this).html());
		});
		$('#catparentlocation').change(function(){
			var locid = $(this).val();
			$.ajax({
				url:'engine/location.php',
				data:'CityID='+locid,
				dataType:'json',
				type:'POST'
			}).done(function(result){
				if(locid>0)
				{
					var opt = $('#catparentlocation option:selected').html();
					$('#catchildlocation').html('').append('<option value="0">All '+opt+'</option>');
				}
				else
				{
					$('#catchildlocation').html('').append('<option value="0">All Locations</option>');
				}
				if(result.length){
					$('#catchildlocation').prop('disabled',false);
					$.each(result,function(i,d){
						$('#catchildlocation').append('<option value="'+d['SubCityID']+'">'+d['SubCity']+'</option>');
					});
				}
				else
				{
					$('#catchildlocation').prop('disabled',true);
				}
				if($('#bottomsearchBoxContainer #catchildlocation').length)
				{
					$('#bottomsearchBoxContainer #catchildlocation').html($('#catchildlocation').clone().html());
				}
			});			
		});
		$('#DoSearch').click(function(event){
			var param = $('#seachForm').serializeArray();
			var cookie = JSON.stringify(param);
			$.cookie('csearch', cookie,{path: '/'});
			$('#content').fadeOut('slow',function(){
				$.ajax({
					url:'engine/jobs.php',
					type:'post',
					data:param,
					dataType:'html',
				}).done(function(content){
					$('#content').html(content).fadeIn('slow');
				});
			});			
			
			if($(this).data('shrink')!==true)
			{
				$('#ToggleFewerOptionsTop').click();
				$('#searchBoxContainer label').hide();
				$('#searchBoxContainer .mod-field').css('min-height','0');
				$('#DoSearch').val("NEW \nSEARCH").after('<a class="clear-form" href="#">Clear Form</a>');
				$('div.l-column').css('width','28%');
				$('div.mod-submit-search').insertAfter('div.l-column:last').css('width','10%').css('float','right').css('margin-left','10px');
				$('#recruiters,#employers').val('0');
				$('#DoSearch').data('shrink',true);
			}
			event.preventDefault();
		});
		$('a.applynow').live('click',function(event){
			var param = 'target='+$(this).prop('target');
			$('#content').load('engine/apply.php',param);
			if($('#DoSearch').data('shrink')!==true)
			{
				$('#ToggleFewerOptionsTop').click();
				$('#searchBoxContainer label').hide();
				$('#searchBoxContainer .mod-field').css('min-height','0');
				//$('#recruiters,#employers,p.state-floatleft').hide();
				$('#DoSearch').val("NEW \nSEARCH").after('<a class="clear-form" href="#">Clear Form</a>');
				$('div.l-column').css('width','28%');
				$('div.mod-submit-search').insertAfter('div.l-column:last').css('width','10%').css('float','right').css('margin-left','10px');
				$('#DoSearch').data('shrink',true);
			}
			$('html, body').animate({scrollTop:0}, 'slow');
			event.preventDefault();
		});
		$('a.addetail').live('click',function(event){		
			var param = 'target='+$(this).prop('target');
			$('#content').load('engine/addetail.php',param);
			if($('#DoSearch').data('shrink')!==true)
			{
				$('#ToggleFewerOptionsTop').click();
				$('#searchBoxContainer label').hide();
				$('#searchBoxContainer .mod-field').css('min-height','0');
				//$('#recruiters,#employers,p.state-floatleft').hide();
				$('#DoSearch').val("NEW \nSEARCH").after('<a class="clear-form" href="#">Clear Form</a>');
				$('div.l-column').css('width','28%');
				$('div.mod-submit-search').insertAfter('div.l-column:last').css('width','10%').css('float','right').css('margin-left','10px');
				$('#DoSearch').data('shrink',true);
			}
			$('html, body').animate({scrollTop:0}, 'slow');
			event.preventDefault();
		});
		$('.moreAdvertiser a').live('click',function(event){		
			$('#searchBoxContainer select').val(0).change();
			$('#searchBoxContainer input:not(.btn)').val('');
			var advertiser = $('<input type="hidden" name="advertiser" value="'+$(this).prop('target')+'"/>');
			$('#seachForm').prepend(advertiser);
			$('#DoSearch').click();
			advertiser.remove();
			event.preventDefault();
		});
		$('.clear-form').live('click',function(event){
			$('#searchBoxContainer select, #bottomsearchBoxContainer select').val(0).change();
			$('#searchBoxContainer input:not(.btn),#bottomsearchBoxContainer input:not(.btn)').val('');
			event.preventDefault();
		});
		$('#bottomsearchBoxContainer #DoSearch').live('click',function(){
			$('#DoSearch').click();
			$('html, body').animate({scrollTop:0}, 'slow');
		});
		$('#bottomsearchBoxContainer #catindustry').live('change',function(){
			$('#searchBoxContainer #catindustry').val($(this).val()).change();
		});
		$('#bottomsearchBoxContainer #catworktype').live('change',function(){
			$('#searchBoxContainer #catworktype').val($(this).val()).change();
		});
		$('#bottomsearchBoxContainer #catoccupation').live('change',function(){
			$('#searchBoxContainer #catoccupation').val($(this).val()).change();
		});
		$('#bottomsearchBoxContainer #catparentlocation').live('change',function(){
			$('#searchBoxContainer #catparentlocation').val($(this).val()).change();
		});
		$('#bottomsearchBoxContainer #catchildlocation').live('change',function(){
			$('#searchBoxContainer #catchildlocation').val($(this).val()).change();
		});
		$('#bottomsearchBoxContainer #Keywords').live('keypress',function(){
			$('#searchBoxContainer #Keywords').val($(this).val()).change();
		});
	})(jQuery);
</script>