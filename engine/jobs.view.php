<?php if($this->pagination['totalpage']>0) : ?>
<div class="jobsearch-index grid_12 mod-section l-clearfix l-row">
                
<div class="grid_3 l-column mod-callout state-pull-out state-searchresults-refine">                       
    <h3>REFINE SEARCH OPTIONS</h3>   
    <dl class="worktype-refine">
        <dt>Work Type</dt>
			<?php foreach($this->workTypeList as $workType):?>
				<dd>
					<input type="checkbox" 					
						checked="checked" 
						value="<?=$workType->employmenttype_valueid?>" class="cb-worktype" id="cb_<?=$workType->employmenttype_valueid?>">
					<label for="cb_<?=$workType->employmenttype_valueid?>"><?=$workType->employmenttype?></label><a class="worktyperefine" href="#" target="<?=$workType->employmenttype_valueid?>">only</a>
				</dd>
			<?php endforeach;?>            
        
    </dl>        
			<?php if(count($this->locationList)>1): ?>
            <dl>
                <dt>Area</dt>
					<?php foreach($this->locationList as $k => $list):?>
                    <dd class="<?php echo $k>2?'less':'';?>" style="<?php echo $k>2?'display:none;':'';?>">
                        <a href="#" target="<?=$list->location_valueid?>" class="location"><?=$list->location?></a>
                        <span>(<?=$list->count_location?>)</span>
                    </dd>
                    <?php endforeach;?> 
                    <?php if(count($this->locationList)>3):?>
                    <dd class="more"><a href="#">more</a><a href="#" style="display:none;">less</a></dd>
					<?php endif;?>                
            </dl>
			<?php else: ?>
				<?php if(count($this->areaList)>1): ?>
				<dl>
					<dt>Location</dt>
						<?php foreach($this->areaList as $k => $list):?>
						<dd class="<?php echo $k>2?'less':'';?>" style="<?php echo $k>2?'display:none;':'';?>">
							<a href="#" target="<?=$list->area_valueid?>" class="area"><?=$list->area?></a>
							<span>(<?=$list->count_area?>)</span>
						</dd>
						<?php endforeach;?> 
						<?php if(count($this->areaList)>3):?>
						<dd class="more"><a href="#">more</a><a href="#" style="display:none;">less</a></dd>
						<?php endif;?>                
				</dl>
				<?php endif;?>
			<?php endif;?>			
			<?php if(count($this->industryList)>1):?>
            <dl>
                <dt>Job Sector</dt>
					<?php foreach($this->industryList as $k => $list):?>
                    <dd class="<?php echo $k>2?'less':'';?>" style="<?php echo $k>2?'display:none;':'';?>">
                        <a href="#" target="<?=$list->classification_valueid?>" class="classification"><?=$list->classification?></a>
                        <span>(<?=$list->count_classification?>)</span>
                    </dd>
					<?php endforeach;?>    
					<?php if(count($this->industryList)>3):?>
                    <dd class="more"><a href="#">more</a><a href="#" style="display:none;">less</a></dd>
					<?php endif;?>
            </dl>
			<?php else: ?>
				<?php if(count($this->occupationList)>1): ?>
				<dl>
					<dt>Job Title</dt>
						<?php foreach($this->occupationList as $k => $list):?>
						<dd class="<?php echo $k>2?'less':'';?>" style="<?php echo $k>2?'display:none;':'';?>">
							<a href="#" target="<?=$list->position?>"  class="position"><?=$list->position?></a>
							<span>(<?=$list->count_position?>)</span>
						</dd>
						<?php endforeach;?> 
						<?php if(count($this->occupationList)>3):?>
						<dd class="more"><a href="#">more</a><a href="#" style="display:none;">less</a></dd>
						<?php endif;?>                
				</dl>
				<?php endif;?>
			<?php endif;?>
		</div>
                
<div class="grid_7 l-column state-searchresults-tools">
    <h1 class="state-searchresults-title"><span><strong>
        <?php echo $this->pagination['total'];?>
		<?=$this->searchlist?></strong>
        jobs <?=$this->searchBy?> in <strong><?=$this->location?></strong></span>
    </h1>
    <div class="mod-callout state-lighter-callout">       
        <a href="http://www.jobstoemail.com/" target="_blank" class="modals" data-target="#lightboxModal" id="SaveJobMailLink"><i class="ico ico-23 ico-email"></i> Email Job Alerts</a>
    </div>
</div>
<div class="grid_7 l-column state-searchresults-container">

    <form method="get" id="SaveJobsForm">
    <ol>
<?php foreach($this->jobs as $job): ?>        
<li class="mod-searchresult-entry">   
    <dl class="l-clearfix">
        <dd>
            <h2>
                <a href="#" target="<?=$job->PrimaryKey?>" class="addetail"><?=$job->position?></a>
                
                <em><?=$job->advertisername?></em>
            </h2>            
            <p><?=substr(strip_tags($job->description),0,100)?>...</p>
            <div class="state-fixedtobase">  
                <div>
                            <span>&gt;</span>
                            <span class="mod-classifiers"><?=$job->classification?></span>
                    		<i class="mod-arrow state-arrow-right"></i>
                            <span class="mod-classifiers"><?=$job->employmenttype?></span>
                    </div>   
<div style="float:left; margin: .5em 0">					
<a href="#" target="<?=$job->PrimaryKey?>" class="applynow">
    <img src="images/apply30.png" alt/>
</a>                
<a href="#" target="<?=$job->PrimaryKey?>" class="addetail">
    <img src="images/detail30.png" alt/>
</a> 
</div>               
            </div>
        </dd>
        
        <dd class="state-has-logo">
            <small>&nbsp;</small>            
            <span><?=$job->location?><i class="mod-arrow state-arrow-right"></i><?=$job->area?></span>                      
			<?php				
				$symbol = '';
				$currency = explode('.',$job->salarycurrency);
				if(count($currency)>1)
					$symbol = $currency[1];
				$start = $job->startdate;
				$startarr = explode('T',$job->startdate);
				if(count($startarr)>1)
					$start = $startarr[0];
				
			?>
			<?php if(isset($job->salaryminimum) && $job->salaryminimum!=''):?><span>Salary Min: <?php echo $symbol.' '.number_format($job->salaryminimum);?></span><?php endif;?>
			<?php if(isset($job->salarymaximum) && $job->salarymaximum!=''):?><span>Salary Max: <?php echo $symbol.' '.number_format($job->salarymaximum);?></span><?php endif;?>
			<?php if(isset($job->salaryadditional) && $job->salaryadditional!=''):?><span>Salary: <?php echo $job->salaryadditional;?></span><?php endif;?>
			<?php if(isset($start) && $start!=''):?><span>Start: <?php echo $start;?></span><?php endif;?>
			<?php if(isset($job->duration) && $job->duration!=''):?><span>Duration: <?php echo $job->duration;?></span><?php endif;?>
        </dd>
    </dl>
    
    <div style="background: url(images/<?=$job->advertisername?>) no-repeat bottom right;" class="mod-standout-logo"></div>
</li>
<?php endforeach; ?>        
</ol>   
</form> 
<?php if($this->pagination['totalpage']>1) : ?>
<div class="grid_7 mod-pagination l-clearfix">
    
    <dl>
        <dt>Page</dt>
			<?php if(isset($this->pagination['prev']) && $this->pagination['prev']==true):?>
				<dd class="nextPage">
					<a class="gotoprev btn state-btn-small state-btn-cta" href="#"><strong>Prev<span>&nbsp;</span></strong></a>
				</dd>
			<?php endif;?>
			<?php for($i = $this->pagination['startpage']; $i <= $this->pagination['endpage']; $i++) : ?>
				<?php if($this->pagination['currentpage']==$i):?>
					<dd class="currentPage"><span><?=$i?></span></dd>                
				<?php else: ?>
					<dd>
						<span>
							<a class="gotopage" href="#"><?=$i?></a>
						</span>
					</dd>
				<?php endif;?>
			<?php endfor;?>
			<?php if(isset($this->pagination['next']) && $this->pagination['next']==true):?>
				<dd class="nextPage">
					<a class="gotonext btn state-btn-small state-btn-cta" href="#"><strong>Next<span>&nbsp;</span></strong></a>
				</dd>
			<?php endif;?>
    </dl>
    
</div>
<?php endif;?>

    <div class="mod-callout state-lighter-callout">
        <a href="http://www.jobstoemail.com/" target="_blank" class="modals" data-target="#lightboxModal" id="SaveJobMailLink"><i class="ico ico-23 ico-email"></i> Email Job Alerts</a>
    </div>


                </div>         
                                	
            <div class="grid_2 l-column-overflow">
                    
				<div id="skycraper" class="mod-skyscraper-banner">
					<iframe src="ads.php/1/<?=$this->searchlist?> jobs <?=$this->searchBy?> in <?=$this->location?>" scrolling="no" height="610" width="170" style="border:none;"></iframe>
				</div>
            </div>        
        </div>
	<div class="mod-search-form l-row is-collapsed state-mini-search" id="bottomsearchBoxContainer"></div>
    <!--Leaderboard banner-->
    
    <div class="mod-section l-row mod-leaderboard-banner" id="leaderboard"> 		
		<iframe src="ads.php/2/<?=$this->searchlist?> jobs <?=$this->searchBy?> in <?=$this->location?>" scrolling="no" height="100" width="738" style="border:none;"></iframe>
    </div>
<?php else:
	require_once('./nofound.view.php');?>
	<div class="mod-search-form l-row is-collapsed state-mini-search" id="bottomsearchBoxContainer"></div>	
<?php endif;?>	
<script>
$('#bottomsearchBoxContainer').html($('#searchBoxContainer').clone().html());
$('#searchBoxContainer select').each(function(k,v){
	$('#bottomsearchBoxContainer #'+v.id).val($('#searchBoxContainer #'+v.id).val());
});
$('#bottomsearchBoxContainer #Keywords').val($('#searchBoxContainer #Keywords').val());
$('.more').click(function(){
	$(this).siblings().filter('.less').toggle();
	$(this).children().toggle();
});
$('.worktyperefine').click(function(event){
	var label = $(this).siblings().filter('label').html();
	var param = 'worktype='+$(this).prop('target')+'&txt='+label;
	$('#content').load('engine/jobs.php',param);
	$('#catworktype').val($(this).prop('target'));
	event.preventDefault();
});
$('.gotopage').click(function(event){
	var param = 'page='+$(this).html();
	$('#content').load('engine/jobs.php',param);
	$('html, body').animate({scrollTop:0}, 'slow');
	event.preventDefault();
});
$('.gotonext').click(function(event){
	$('dd.currentPage').next().find('a').click();
	$('html, body').animate({scrollTop:0}, 'slow');
	event.preventDefault();	
});
$('.gotoprev').click(function(event){
	$('dd.currentPage').prev().find('a').click();
	$('html, body').animate({scrollTop:0}, 'slow');
	event.preventDefault();	
});
$('a.location').click(function(event){
	$('#catparentlocation').val($(this).prop('target')).change();
	$('#DoSearch').click();
	event.preventDefault();
});
$('a.area').click(function(event){
	$('#catchildlocation').val($(this).prop('target')).change();
	$('#DoSearch').click();
	event.preventDefault();
});
$('a.classification').click(function(event){
	$('#catindustry').val($(this).prop('target')).change();
	$('#DoSearch').click();
	event.preventDefault();
});
$('a.position').click(function(event){
	$('#catoccupation').val($(this).prop('target')).change();
	$('#DoSearch').click();
	event.preventDefault();
});
$('.cb-worktype').change(function(){
	var val = new Array();
	$('.cb-worktype:checked').each(function(k,v){
		val.push($(v).val());
	});
	var param = 'arrworktype='+JSON.stringify(val);
	$('#content').load('engine/jobs.php',param);
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-12247273-10']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>