<?php
$host = explode('/',ltrim($_SERVER["SCRIPT_NAME"],'/')); array_pop($host); $host = '/'.implode('/',$host);
if(isset($_GET['job']))
{
	require_once('engine/model.php');
	class JobDetailModel extends Model
	{
		public function getPKbyReference($reference)
		{
			$sql = "SELECT `PrimaryKey` FROM xml_jobg8_oz WHERE senderreference=%d";
			$obj = $this->queryObject(sprintf($sql,$reference));
			return $obj->PrimaryKey;
		}
	}
	$ref = base64_decode($_GET['job']);
	$jobDetailModel = new JobDetailModel();
	$_GET['target'] = $jobDetailModel->getPKbyReference($ref);
}	
else if(isset($_GET['apply']))
	$_GET['target'] = base64_decode($_GET['apply']);
else if(isset($_GET['list']))
{	
	require_once('engine/model.php');
	class JobListModel extends Model
	{
		public function SearchByEmail($id)
		{
			$sql = "SELECT 
					`YourName`, `email`, `JobCategoryID`, `JobSubCategoryID`, 
					`CountryID`, `LocationID`, `SubLocationID`, `Confirmed`, 
					COALESCE(b.`sub_classification`,'0') AS sub_classification,
					COALESCE(c.classification,'All Job Sectors') AS classification,
					COALESCE(d.country,'All Countries') AS country, 
					COALESCE(e.location,'All Areas') AS location,
					COALESCE(f.area,'All Locations') AS area 
				FROM 
					`email_jobs` LEFT JOIN 
					`sub_classification` b ON `email_jobs`.`JobSubCategoryID`=b.sub_classification_valueid LEFT JOIN
					`classification` c ON `email_jobs`.`JobCategoryID`=c.`classification_valueid` LEFT JOIN
					`country` d ON `email_jobs`.`CountryID`=d.`country_valueid` LEFT JOIN
					`location` e ON `email_jobs`.`LocationID`=e.location_valueid LEFT JOIN
					`area` f ON `email_jobs`.`SubLocationID`=f.area_valueid
					WHERE `PrimaryKey`=%d";
					
			return $this->queryObject(sprintf($sql,$id));
		}
	}
	$_POST = array();
	$_POST['Keywords'] = '';
	$search = new JobListModel();
	$byemail = $search->SearchByEmail(base64_decode($_GET['list']));
	$_POST['worktype'] = '0';
	$_POST['occupation'] = $byemail->sub_classification;
	$_POST['recruiters'] = '0';
	$_POST['employers'] = '0';
	$_POST['country_str'] = $byemail->country;
	$_POST['parentlocation_str'] = $byemail->location;
	$_POST['childlocation_str'] = $byemail->area;
	$_POST['parentlocation'] = $byemail->LocationID;
	$_POST['countries'] = $byemail->CountryID;
	$_POST['childlocation'] = $byemail->SubLocationID;
	$_POST['industry'] = $byemail->JobCategoryID;
	$_POST['industry_str'] = $byemail->classification;
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Oz Jobs Online - For all the Jobs in Australia</title>
    <meta name='Distribution' content='Global' />
    <link href="site.css" rel="stylesheet" type="text/css" />
	<link href="site2.css" rel="stylesheet" type="text/css" />
	<link href="images/style.css" rel="stylesheet" type="text/css" />
    <meta name="Author" content="Globo Logic." />
    <meta name='Copyright' content='&copy; Globo Logic Pty Ltd' />
    <meta name='Robots' content='ALL' />
    <meta name='Revisit-after' content='4 days' />
    <meta name='Keywords' content="New Zealand, NZ Jobs, work in NZ, NZ work, Kiwi Jobs,Pommy, British, poms, united kingdon, uk jobs, jobs in uk, work in uk, uk work,Yankee Jobs, United States jobs, jobs, job, australia, sydney, melbourne, brisbane, perth, adelaide, darwin, hobart, canberra, auckland, wellington, christchurch, employment, where to find work,
	australian jobs, work in perth, work in australia" />
    <meta name="Description" content="For all the jobs in Australia use Oz Jobs Online" />
    <meta name="abstract" content="" />
    <script language="JavaScript" type="text/javascript" src="jquery-1.7.2.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="jquery.cookie.js"></script>
	<script language="JavaScript" type="text/javascript" src="json.js"></script>
    <style type="text/css">
        .modalBackground {
	        background-color: Gray;
	        filter: alpha(opacity=70);
	        opacity: 0.7;
        }
    </style>
	<script language="javaScript" type="text/javascript"> var message="Our website is Copyrighted. Trying to see our source or steal images is illegal"; function clickIE4(){ if (event.button==2){ alert(message); return false; } } function clickNS4(e){ if (document.layers||document.getElementById&&!document.all){ if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false") </script>
</head>
<body class="FourColFixHdr">    
    <div id="container">
        <div id="header">
            <table width="100%">
                <tr>
                    <td align="left" style="padding: 10px 0px 10px 0px;">
						<div id="shortcut">
<div class="mod-field">
							 <?php require_once('more_lists.html') ;?>							
							</div>
							</div>
                        <img src="images/logo.jpg" />Number one place for all your Jobs in Australia
                    </td>
                </tr>
            </table>
            <div style="width:1020px;" id="searchjobs">		
				<?php require_once('./engine/search.php') ;?>
			</div>
			<script language="JavaScript" type="text/javascript">		
				if($('#DoSearch').data('shrink')!==true)
				{
					$('#ToggleFewerOptionsTop').click();
					$('#searchBoxContainer label').hide();
					$('#searchBoxContainer .mod-field').css('min-height','0');
					$('#DoSearch').val("NEW \nSEARCH").after('<a class="clear-form" href="#">Clear Form</a>');
					$('#searchBoxContainer div.l-column').css('width','28%');
					$('#searchBoxContainer div.mod-submit-search').insertAfter('div.l-column:last').css('width','10%').css('float','right').css('margin-left','10px');
					$('#recruiters,#employers').val('0');
					$('#DoSearch').data('shrink',true);
				}	
				$('#shortcut').siblings().filter('img').css('cursor','pointer').click(function(){window.location.replace('<?=$host?>');});	
				$(document).ready(function(){$('body').fadeIn('slow');});
			</script>
        </div>
		<div id="content">
			<?php 
			if(isset($_GET['job']))
				include('engine/addetail.php');
			else if(isset($_GET['apply']))
				include('engine/apply.php');
			else if(isset($_GET['list']))
			{
				unset($_GET['list']);
				include('engine/jobs.php');
			}
			?>
		</div>		
		<!-- ****************************** START FOOTER  ********************************* -->
<?php require_once('detail_footer.html') ;?> 
<!-- end #container -->
</body>
</html>