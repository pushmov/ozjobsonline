<div class="jobsearch-index mod-section state-section-page l-clearfix l-row">                   
    <div class="no-results">
        <h1><i>0 jobs found for</i> - <span><strong>
		<?=$this->searchlist?>
		</strong>
        jobs in <strong><?=$this->location?></strong></span></h1>        
        <div class="why-nothing">
        <h2>What went wrong?</h2>
        <ol>
             <li><h2>No jobs found</h2>
            <p>Your search has resulted in no jobs being found.<br>
             <a href="http://www.jobstoemail.com/" target="_blank">If you would like to be notified of any jobs matching your search join up for a email Job Alert.</a></p>
             <a class="grey-btn-lge" href="http://www.jobstoemail.com/" target="_blank"><strong><img width="27" height="24" alt="" src="images/icons_sm_envelope.png">Job Alert</strong></a>
             </li>
             <li><h2>Could be a typo</h2><p>If you used keywords - check the spelling, make sure you have spelt it correctly!</p></li>
            <li><h2>Check your Selection</h2><p>Make sure that you have chosen the correct search options.</p></li>
        </ol>
        </div>
    </div>
	<br>
</div>