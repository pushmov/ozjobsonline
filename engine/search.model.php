<?php
require_once('model.php');
class SearchModel extends Model
{
	public function getClassificationOptions()
	{
		$sql = "SELECT DISTINCT
			`classification` AS Classification1Name , 
			`classification_valueid` AS Classification1ID 
			FROM `xml_jobg8_oz` ORDER BY `classification`";
		return $this->query($sql);
	}
	
	public function getSubClassificationOptions()
	{
		$sql = "SELECT DISTINCT 
			`position` AS Classification2ID , 
			`position` AS Classification2Name ,
			`classification_valueid`
			FROM `xml_jobg8_oz` ORDER BY `position`";
		return $this->query($sql);
	}
	
	public function getLocationOptions()
	{	
		$sql = "SELECT DISTINCT
				`location` AS City , 
				`location_valueid` AS CityID 
			FROM `xml_jobg8_oz` ORDER BY `location`";
		return $this->query($sql);
	}
	
	public function getWorkTypeOptions()
	{
		$sql = "SELECT DISTINCT
				`employmenttype` , 
				`employmenttype_valueid` 
			FROM `xml_jobg8_oz` ORDER BY `employmenttype`";
		return $this->query($sql);
	}
	
	public function getRecruitersOptions()
	{
		$sql = "SELECT DISTINCT
				`advertisername` 
			FROM `xml_jobg8_oz` WHERE `advertisertype_valueid` IN (15890,15891) ORDER BY `advertisername`";
		return $this->query($sql);
	}
	
	public function getEmployersOptions()
	{
		$sql = "SELECT DISTINCT
				`advertisername` 
			FROM `xml_jobg8_oz` WHERE `advertisertype_valueid` IN (15892) ORDER BY `advertisername`";
		return $this->query($sql);
	}
}